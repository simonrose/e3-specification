e3-specification
================

A proof-of-concept Python package for e3.

The specification is primarily intended to define modules to be built for an environment.

Quickstart
----------

This package requires Python 3.6 or later.

.. code-block:: sh

    $ pip3 install e3-specification -i https://artifactory.esss.lu.se/artifactory/api/pypi/pypi-virtual/simple
    $ pv-simple-validator -h


Contributing
------------

Contributions through pull/merge requests only.
